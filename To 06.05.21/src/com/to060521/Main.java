package com.to060521;

import lombok.extern.java.Log;

import java.util.*;
import java.util.function.Predicate;

@Log
public class Main {
    public static void main(String[] args) {
        log.info("Task 1 started started");
        TreeSet<String> treeSet = new TreeSet<>(new Comparrrator());
        treeSet.add("Hello");
        treeSet.add("bla");
        treeSet.add("Qwerty");
        treeSet.add("Itis");
        log.info(treeSet.toString());
        log.warning("Task 1 ended");
        log.info("Task 2 started");
        int[] a = new int[] {new Random().nextInt(), new Random().nextInt(),
                            new Random().nextInt(), new Random().nextInt()};
        Predicate<Integer> predicate = i -> i % 2 == 0;
        log.info("Origin version: " + Arrays.toString(a));
        a = method(a, predicate);
        log.info("Last version: " + Arrays.toString(a));
        log.warning("Task 2 ended");
        log.info("Task 3 started");
        Product product1 = new Product("Cola", new Random().nextInt(100));
        Product product2 = new Product("Pepsi", new Random().nextInt(100));
        Product product3 = new Product("Cheese", new Random().nextInt(100));
        Product.addProduct(product1);
        Product.addProduct(product2);
        Product.addProduct(product3);
    }

    public static int[] method(int[] a, Predicate<Integer> predicate) {
        int[] ar = new int[a.length];
        int cnt = 0;
        for (Integer i:
             a) {
            if (predicate.test(i)) {
                ar[cnt] = i;
                cnt++;
            }
        }
        int[] array = new int[cnt];
        System.arraycopy(ar, 0, array, 0, cnt);
        return array;
    }
}

class Comparrrator implements Comparator<String> {
    public int compare(String s, String k) {
        return Integer.compare(s.length(), k.length());
    }
}
interface ProductListener {
    void productsUpdated();
}

@Log
record Product(String name, int amount) implements ProductListener{
    private static ArrayList<ProductListener> listeners = new ArrayList<>();
    private static ArrayList<Product> products = new ArrayList<>();
    public static void addListener(ProductListener productListener) {
        listeners.add(productListener);
        for (ProductListener p: listeners) {
            p.productsUpdated();
        }
    }
    public static void addProduct(Product product) {
        products.add(product);
        for (Product p: products) {
            p.productsUpdated();
        }
    }

    @Override
    public void productsUpdated() {
        log.info(products.toString());
        int cnt = 0;
        for (Product product: products) {
            cnt += product.amount();
        }
        log.info("Total place taken: " + cnt);
        log.info(Objects.requireNonNull(products.stream().max(Comparator.comparingInt(Product::amount)).orElse(null)).toString());
    }
}
