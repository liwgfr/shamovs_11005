package com.threads;

import lombok.SneakyThrows;

import java.io.*;
import java.security.InvalidParameterException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Problems {
}

class Problem1 {
    static volatile long sum = 1;
    static volatile int iteration = 1;
    @SneakyThrows
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Thread t1 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                while (iteration < n) {
                synchronized (this) {
                        iteration++;
                        sum *= iteration;
                        System.out.println(Thread.currentThread().getName() + " " + sum);
                        Thread.sleep(1500);
                    }
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                while (iteration < n) {
                synchronized (this) {
                        iteration++;
                        sum *= iteration;
                        System.out.println(Thread.currentThread().getName() + " " + sum);
                        Thread.sleep(1500);
                    }
                }
            }
        });
        Thread t3 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                while (iteration < n) {
                synchronized (this) {
                        iteration++;
                        sum *= iteration;
                        System.out.println(Thread.currentThread().getName() + " " + sum);
                        Thread.sleep(1000);
                    }
                }
            }
        });
        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        System.out.println(sum);

    }
}

class Problem2 {
    @SneakyThrows
    public static void main(String[] args) {
        FileReader fileReader = new FileReader("/home/binocla/IdeaProjects/shamovs_11005/Lessons/src/com/threads/input.txt");
        Scanner in = new Scanner(fileReader);
        List<String> allStrings = new ArrayList<>();
        List<String> uniqueChet = new ArrayList<>();
        List<String> uniqueNechet = new ArrayList<>();
        while (in.hasNext()) {
            allStrings.add(in.next().toLowerCase(Locale.ROOT));
        }
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    allStrings.stream().filter(x -> x.chars().distinct().count() % 2 == 0).forEach(x -> {
                        uniqueChet.add(x);
                        System.out.println(Thread.currentThread().getName() + " added chet");
                    });
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                    synchronized (this) {
                        allStrings.stream().filter(x -> x.chars().distinct().count() % 2 != 0).forEach(x -> {
                            uniqueNechet.add(x);
                            System.out.println(Thread.currentThread().getName() + " added nechet");
                        });
                    }
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(uniqueChet);
        System.out.println(uniqueNechet);
    }
}

class Problem3 {
    static volatile int sum = 0;
    @SneakyThrows
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int []ar = new int[n];
        for (int i = 0; i < n; i++) {
            ar[i] = in.nextInt();
        }
        for (int i = 0; i < n; i++) {
            int finalI = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (this) {
                        sum += ar[finalI];
                        System.out.println(Thread.currentThread().getName() + " " + sum);
                    }
                }
            });
            t.start();
            t.join();
        }
        System.out.println(sum);
    }
}

class Problem1Actual {
        static volatile long fact = 1;
        static volatile int iteration = 0;
        @SneakyThrows
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int n = in.nextInt();
            while (iteration < n) {
                CountDownLatch latch = new CountDownLatch(5);
                Thread thread = new Thread(new Runnable() {
                    @SneakyThrows
                    @Override
                    public void run() {
                        while (latch.getCount() > 0 && iteration < n) {
                            synchronized (this) {
                                iteration++;
                                fact *= iteration;
                                Thread.sleep(1500);
                                System.out.println(Thread.currentThread().getName() + " current res is " + fact);
                                latch.countDown();
                            }
                        }
                    }
                });
                thread.start();
                thread.join();
            }
            System.out.println(fact);
        }
}

class Problem4 {
    static AtomicBoolean flag = new AtomicBoolean(false);
    static FileOutputStream fileOutputStream;

    static {
        try {
            fileOutputStream = new FileOutputStream("save.dat");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static ObjectOutputStream objectOutputStream;

    static {
        try {
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static FileInputStream fileInputStream;

    static {
        try {
            fileInputStream = new FileInputStream("save.dat");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static ObjectInputStream objectInputStream;

    static {
        try {
            objectInputStream = new ObjectInputStream(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws InterruptedException {

        final WriteAndRead writeAndRead = new WriteAndRead();
        Thread t1 = new Thread(() -> {
                try {
                    writeAndRead.write();
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
        });
        Thread t2 = new Thread(() -> {
            try {
                writeAndRead.read();
            } catch (InterruptedException | IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        t1.setName("Writer");
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }
    public static class WriteAndRead {
            public void write() throws InterruptedException, IOException {
                for (int i = 0; i < 10; i++) {
                    synchronized (this) {
                        while (flag.get())
                            wait();
                        objectOutputStream.writeObject(new Randomizer().nextRandom());
                        System.out.println(Thread.currentThread().getName() + " successfully wrote");
                        flag.set(true);
                        notify();
                        Thread.sleep(1000);
                    }
                }
            }
            public void read() throws InterruptedException, IOException, ClassNotFoundException {
                for (int i = 0; i < 10; i++) {
                    synchronized (this) {
                        while (!flag.get())
                            wait();
                        System.out.println(objectInputStream.readObject());

                        flag.set(false);
                        notify();
                        Thread.sleep(1000);
                    }
                }
                objectOutputStream.close();
            }
    }
}

record Person(String name, int age, Countries country) implements Serializable { }

enum Countries {
    RUSSIA,
    SWEDEN,
    CHINA,
    ITALY,
    GERMANY,
    BINOCLA,
    ITIS,
    ANDORRA,
    SPAIN,
    DENMARK,
    DC,
    MARVEL,
    APPLE,
    SAMSUNG,
    DELL,
    STEAM,
    ORIGIN,
    JAVA,
    C_PLUS {
        @Override
        public String toString() {
            return "C++";
        }
    },
    C_SHARP {
        @Override
        public String toString() {
            return "C#";
        }
    }
}
sealed class GlobalRandom permits Randomizer {
    public Object nextRandom() {
        return new Object();
    }
}
final class Randomizer extends GlobalRandom {
    @Override
    public Person nextRandom() {
        return new Person(UUID.randomUUID().toString(), new Random().nextInt(100), Countries.values()[new Random().nextInt(Countries.values().length)]);
    }
}

class Problem5 {
    @SneakyThrows
    public static void main(String[] args) {
        StringBuilder s = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(bufferedReader.readLine());
        if (n < 0) {
            throw new InvalidParameterException("Вы ввели отрицательное число!");
        }
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
        for (int i = 0; i < n; i++) {
            Callable<StringBuilder> task = () -> {
                boolean flag = new Random().nextBoolean();
                for (int j = 0; j < new Random().nextInt(6) + 1; j++) {
                    if (flag) {
                        s.append(((char) (new Random().nextInt('z' - 'a') + 'a')));
                    } else {
                        s.append((char) (new Random().nextInt('я' - 'а') + 'а'));
                    }

                }
                System.out.println(Thread.currentThread().getName() + " has added new chars to String!");
                return s;
            };
            Future<StringBuilder> future = scheduledExecutorService.schedule(task, 3, TimeUnit.SECONDS);
            System.out.println(future.get());
        }
        scheduledExecutorService.shutdown();
        if (scheduledExecutorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
            scheduledExecutorService.shutdownNow();
        }
    }
}
class Problem6 {
    final static Scanner in = new Scanner(System.in);
    static int n;
    static String answer = "";

    static volatile int l = 2;
    static AtomicInteger left = new AtomicInteger(0);
    static AtomicInteger right = new AtomicInteger(10);

    @SneakyThrows
    public static void main(String[] args) {
        n = in.nextInt();
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        AtomicInteger j = new AtomicInteger(l);
        while (left.get() <= n) {
            executorService.submit(() -> {
                for (int i = l; i <= n; i++) {
                    for (j.set(l); (i % j.get()) > 0; j.incrementAndGet()) {
                    }
                    if (i == j.get() && i >= left.get() && i <= right.get()) {
                        answer += i + "\n";
                    }
                }
                left.set(right.get() + 1);
                right.set(right.get() + 10);
                System.out.println(Thread.currentThread().getName() + " is " + answer);
                return answer;
            });
            Thread.sleep(1500);
        }
        executorService.shutdown();
        if (executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
            executorService.shutdownNow();
        }
    }
}
