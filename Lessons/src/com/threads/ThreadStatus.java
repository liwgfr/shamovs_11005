package com.threads;

import lombok.SneakyThrows;

import java.util.Scanner;

class Blabla {
    @SneakyThrows
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            new Thread(() -> System.out.println(Thread.currentThread().getName())).start();

        }
    }
}
