package com.threads;

import lombok.SneakyThrows;

public class StaleValues implements Runnable{
    private volatile boolean asleep; // volatile фиксит!
    public void setAsleep(boolean asleep) {
        this.asleep = asleep;
    }
    @SneakyThrows
    public static void main(String[] args) {
        StaleValues staleValues = new StaleValues();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                staleValues.setAsleep(true);
            }
        });
        Thread thread1 = new Thread(staleValues);

        thread.start();
        thread1.start();

    }

    @Override
    public void run() {
        if (!asleep) {
            System.out.println("Hi");
            // Другой тред не гарантированно будет видеть значения, выставленные внешним тредом
        }
    }
}
