package com.threads;

import lombok.SneakyThrows;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SumWithTwoThreads {
    private static volatile int sum = 0;
    @SneakyThrows
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                while (sum < 100) {
                    synchronized (this) {
                        sum++;
                        System.out.println(Thread.currentThread().getName() + " sum is " + sum);
                        Thread.sleep(100);
                    }
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                while (sum < 100) {
                    synchronized (this) {
                        sum++;
                        System.out.println(Thread.currentThread().getName() + " sum is " + sum);
                        Thread.sleep(100);
                    }
                }
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(sum);
    }
}

class SumWithExecutors {
    private static volatile int sum = 0;
    @SneakyThrows
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                while (sum < 100) {
                    synchronized (this) {
                        sum++;
                        System.out.println(Thread.currentThread().getName() + " sum is " + sum);
                        Thread.sleep(50);
                    }
                }
            }
        });
        executorService.submit(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                while (sum < 100) {
                    synchronized (this) {
                        sum++;
                        System.out.println(Thread.currentThread().getName() + " sum is " + sum);
                        Thread.sleep(50);
                    }
                }
            }
        });
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        System.out.println(sum);


    }
}