package com.threads;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class RaceCondition {
    public static void main(String[] args) {
        DumbCounter dumbCounter1 = new DumbCounter();
        DumbCounter dumbCounter2 = new DumbCounter();


        IntStream.range(0, 1000).forEach(x -> dumbCounter1.increment());
         System.out.println(dumbCounter1.count);

        IntStream.range(0, 1000).parallel().forEach(x -> dumbCounter2.increment());
        //System.out.println(dumbCounter1.count);
         System.out.println(dumbCounter2.count);


        NotVeryDumbCounter notVeryDumbCounter = new NotVeryDumbCounter();
        IntStream.range(0, 1000).parallel().forEach(x -> notVeryDumbCounter.increment());
        System.out.println(notVeryDumbCounter.count);


        CleverCounter cleverCounter = new CleverCounter();
        IntStream.range(0, 1000).parallel().forEach(x -> cleverCounter.increment());
        System.out.println(cleverCounter.count);



        VeryCleverCounter veryCleverCounter = new VeryCleverCounter();
        IntStream.range(0, 1000).parallel().forEach(x -> veryCleverCounter.increment());
        System.out.println(veryCleverCounter.count);
    }


}

class DumbCounter {
    int count;
    void increment() {
        count++;
    }
}

class CleverCounter {
    int count;
    synchronized void increment() {
        count++;
    }
}
class NotVeryDumbCounter {
    volatile int count;
    void increment() {
        count++;
    }

}

class VeryCleverCounter {
    AtomicInteger count = new AtomicInteger(0);
    void increment() {
        count.incrementAndGet();
    }
}
class Hokage {

    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger();

        VeryCleverCounter binoclaLovesJava = new VeryCleverCounter();
        IntStream.rangeClosed(0, 1000).parallel().forEach(x -> binoclaLovesJava.increment());
        System.out.println(binoclaLovesJava.count);
    }
}
