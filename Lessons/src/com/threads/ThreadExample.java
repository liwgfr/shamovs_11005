package com.threads;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.java.Log;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadExample {
    public static void main(String[] args)
            throws InterruptedException {
        final PC pc = new PC();

        Thread t1 = new Thread(() -> {
            try {
                pc.produce();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                pc.consume();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();
    }

    public static class PC {

        List<Integer> list = new ArrayList<>();
        int capacity = 5;
        public void produce() throws InterruptedException {
            int value = 0;
            while (true) {
                synchronized (this) {
                    while (list.size() == capacity)
                        wait();
                    System.out.println("Producer produced-"
                            + value);
                    list.add(value++);

                    notify();

                    Thread.sleep(1000);
                }
            }
        }

        public void consume() throws InterruptedException {
            while (true) {
                synchronized (this) {
                    while (list.size() == 0)
                        wait();
                    int val = list.remove(0);

                    System.out.println("Consumer consumed-"
                            + val);

                    notify();
                    Thread.sleep(1000);
                }
            }
        }
    }
}


class Program {
    static AtomicBoolean aBoolean = new AtomicBoolean(false);
    @SneakyThrows
    public static void main(String[] args) {
        Store store = new Store();
        Producer producer = new Producer(store);
        Consumer consumer = new Consumer(store);
        Thread t1 = new Thread(producer);
        Thread t2 = new Thread(consumer);
        t1.start();
        t2.start();
    }

    static ReentrantLock lock = new ReentrantLock();
    @Log
    static
    class Store {
        private int product = 5;
        public synchronized void get() {
            log.info("Lock is " + lock.toString());
            if (aBoolean.get()) {
                Thread.currentThread().interrupt();
            }
            while (product < 1) {
                try {
                    lock.lock();
                    wait();
                } catch (InterruptedException e) {
                    log.warning(Thread.currentThread().getName() + " was interrupted!");
                    Thread.currentThread().stop();
                } finally {
                    lock.unlock();
                }
            }
            product--;
            log.info("Consumer bought 1 product!");
            log.info(MessageFormat.format("Storage contains {0} products", product));
            notify();
        }

        public synchronized void put() {
            log.info("Lock is " + lock.toString());
            if (aBoolean.get()) {
                Thread.currentThread().interrupt();
            }
            while (product >= 3) {
                try {
                    lock.lock();
                    wait();
                } catch (InterruptedException e) {
                    log.warning(Thread.currentThread().getName() + " was interrupted!");
                    Thread.currentThread().stop();
                } finally {
                    lock.unlock();
                }
            }
            product++;
            log.info("Producer produced 1 product!");
            log.info(MessageFormat.format("Storage contains {0} products", product));
            notify();
        }
    }
        @AllArgsConstructor
        static
        class Producer implements Runnable {
            Store store;
            @SneakyThrows
            @Override
            public void run() {
                while (!aBoolean.get()) {
                    for (int i = 0; i < 5; i++) {
                        store.put();
                    }
                    aBoolean.set(true);
                }
            }
        }
        @AllArgsConstructor
        static
        class Consumer implements Runnable {
            Store store;
            @Override
            public void run() {
                while (!aBoolean.get()) {
                    for (int i = 0; i < 5; i++) {
                        store.get();
                    }
                    aBoolean.set(true);
                }
            }
        }
}
