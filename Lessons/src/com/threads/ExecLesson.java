package com.threads;

import lombok.SneakyThrows;

import java.security.InvalidParameterException;
import java.util.concurrent.*;

class ExecLesson implements Runnable {
    private final String task;

    ExecLesson(String task) {
        this.task = task;
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            System.out.println("Executing "+ task +" with "+Thread.currentThread().getName());
        }
        System.out.println();
    }
}

class Exec2 {

    @SneakyThrows
    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();
        for (int i = 1; i <= 5; i++) {
            Runnable worker = new ExecLesson("Task" + i);
            executor.execute(worker);
        }
        executor.shutdown();
        if (executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
            executor.shutdownNow();
        }
    }
}

class FutureTest implements Callable<Integer> {
    int number;
    public FutureTest(int number) {
        this.number = number;
    }
    @Override
    public Integer call() {
        int fact = 1;
        if (number < 0) {
            throw new InvalidParameterException("Number should be positive");
        }
        for (int i = number; i > 1; i--) {
            fact *= i;
        }
        return fact;
    }

    @SneakyThrows
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(3);
        FutureTest futureTest = new FutureTest(-2);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> future = executorService.submit(futureTest);
        try {
            System.out.println(future.get());
        } catch (ExecutionException e) {
            e.getCause().printStackTrace();
        }
        executorService.shutdown();
        if (executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
            executorService.shutdownNow();
        }
    }
}