package com.threads;

import lombok.SneakyThrows;

public class Reordering {
    static int x = 0, y = 0, a = 0, b = 0;
    @SneakyThrows
    public static void main(String[] args) {
        Thread one = new Thread(() -> {
            a = 1;
            x = b;
        });
        Thread two = new Thread(() -> {
            b = 1;
            y = a;
        });

        one.start();
        two.start();
        one.join();
        two.join();
        // Порядок может быть разным!! Порядок не определен
        // Любая программа без синхронизации заведомо "сломана"
        /*
            Из-за reordering нельзя рассуждать о результате работы одного треда
            с точки зрения другого треда как о промежуточном
            результате выполнения исходного кода
         */
        System.out.println(x + " " + y);
    }
}
