package com.threads;

public class ThreadInterrupting {
    public static int sum = 0;

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread(new MyThread());
            t.start();
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(sum);
    }
}

class MyThread implements Runnable {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " is started");

        for (int i = 0; i < 10; i++){
            int temp = ThreadInterrupting.sum;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            temp++;
            ThreadInterrupting.sum = temp;
        }
    }
}