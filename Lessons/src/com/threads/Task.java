package com.threads;

import lombok.SneakyThrows;
import lombok.extern.java.Log;

@Log
public class Task extends Thread {
    @SneakyThrows
    public static void main(String[] args) {
        Task task = new Task();
        task.start();
        try {
            Thread.sleep(10000);
            task.interrupt();
        } catch (RuntimeException e) {
            System.out.println("Супер, наконец-то это закончилось!");
        }
    }
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                System.out.println("Я запущен!");
            }
        } catch (InterruptedException e) {
            System.out.println("Хорош, я понял, что ты запущен!");
            throw new RuntimeException("Конец");
        }
    }

}
