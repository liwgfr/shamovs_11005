package com.threads;

import lombok.SneakyThrows;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Lock {
    private final ReentrantLock bankLock = new ReentrantLock(); // Thread может заново зайти! (Reentrant)
    private Condition sufficientFunds = bankLock.newCondition();
    private int[] accounts;
    void moneyTransfer(int from, int to, int amount) {
        bankLock.lock();
        try {
            accounts[from]-=amount;
            accounts[to]+=amount;
        } finally {
            bankLock.unlock();
        }
    }

    // Trouble 1
    void transfer(int from, int amount) {
        while (accounts[from] < amount) {
            // wait...
        }


        bankLock.lock();
        try {
            // transfer funds ...
        } finally {
            bankLock.unlock();
        }

        // доступ не синхронизирован. Иначе - все равно кто-то может уменьшить деньги до вхождения transfer funds

    }
    // Trouble 2
    void transfer3(int from, int amount) {
        bankLock.lock();
        try {
            while (accounts[from] < amount) {
                // wait...
            }

            // transfer funds ...
        } finally {
            bankLock.unlock();
        }
    }

    // Fix

    @SneakyThrows
    void transfer2(int from, int amount) {
        bankLock.lock();
        try {
            while (accounts[from] < amount) {
                sufficientFunds.await();
            }
            accounts[from]-=amount;
            sufficientFunds.signalAll();
        } finally {
            bankLock.unlock();
        }

        // никто не может закинуть деньги, потому что держим блок
    }



    // То же самое с intrinsic lock (reentrant более насыщенный методами)
    @SneakyThrows
    synchronized void moneyTransfer1(int from, int to, int amount) {
        while (accounts[from] < amount) {
            wait(); // wait on intrinsic lock condition
        }
        accounts[from]-=amount;
        accounts[to]+=amount;
        notifyAll();
    }

    // Более приятный способ:
    private Object lock = new Object();
    @SneakyThrows
    void moneyTransfer2(int from, int to, int amount) {
        synchronized (lock) {
            while (accounts[from] < amount) {
                lock.wait(); // wait on intrinsic lock condition
            }
            accounts[from] -= amount;
            accounts[to] += amount;
            lock.notifyAll();
        }
    }
}
// DeadLock
class LeftRightDeadLock {
    private final Object left = new Object();
    private final Object right = new Object();
    @SneakyThrows
    void leftRight() {
        synchronized (left) { // Неправильный порядок
            System.out.println(Thread.currentThread().getName() + " holding left");
            Thread.sleep(2000);
            synchronized (right) {
                System.out.println(Thread.currentThread().getName() + " holding right");
                System.out.println("Blabla");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @SneakyThrows
    void rightLeft() {
        synchronized (right) { // Неправильный порядок
            System.out.println(Thread.currentThread().getName() + " holding right");
            Thread.sleep(2000);
            synchronized (left) {
                System.out.println("Haha");
                System.out.println(Thread.currentThread().getName() + " holding left");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        LeftRightDeadLock lock = new LeftRightDeadLock();
        Thread thread1 = new Thread(lock::leftRight);
        Thread thread2 = new Thread(lock::rightLeft);
        thread1.start();
        thread2.start();
    }

    void transferMoney(Integer fromAccount, Integer toAccount, int amount) throws IllegalAccessException {
        // параметры извне. from <-> to
        // fix -> compare ID of accounts;
        // если нельзя -> книжка Гетца =) (например, хэшкоды, если коллизия -> доп лок)
        synchronized (fromAccount) {
            synchronized (toAccount) {
                if (fromAccount.hashCode() < amount) {
                    throw new IllegalAccessException();
                } else {
                    toAccount.intValue();
                    fromAccount.intValue();
                }
            }
        }
    }
}
