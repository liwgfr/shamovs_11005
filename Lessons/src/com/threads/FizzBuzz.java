package com.threads;

import java.util.stream.IntStream;

public class FizzBuzz {
    public static void main(String[] args) {
        IntStream.rangeClosed(1, 100).mapToObj(i -> {
            String str = "FizzBuzz";
            if (i % 15 == 0) {
                return str;
            } else if (i % 3 == 0) {
                return str.substring(0, 4);
            } else if (i % 5 == 0) {
                return str.substring(4);
            }
            return i;
        }).forEach(System.out::println);


        /*
        for (int i = 1; i <= 100; i++) {
            if (i % 15 == 0) {
                System.out.println("FizzBuzz");
                continue;
            }
            if (i % 3 == 0) {
                System.out.println("Fizz");
                continue;
            }
            if (i % 5 == 0) {
                System.out.println("Buzz");
                continue;
            }
            System.out.println(i);

        }

         */
    }
}




