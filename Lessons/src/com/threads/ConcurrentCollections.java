package com.threads;

import lombok.SneakyThrows;
import lombok.extern.java.Log;

import java.util.*;
import java.util.concurrent.*;

public class ConcurrentCollections implements Runnable {
    static volatile int counter = 5;
    static ArrayList<Integer> ar = new ArrayList<>(counter);

    @SneakyThrows
    public static void main(String[] args) {
            Thread t1 = new Thread(new ConcurrentCollections());
            Thread t2 = new Thread(new ConcurrentCollections());
            t1.start();
            t2.start();
            Thread.sleep(500);
            t1.join();
            t2.join();
            t1.interrupt();
            t2.interrupt();
            System.out.println(ar);
    }


    @Override
    public void run() {
        while (counter >= 0) {
            ar.add(new Random().nextInt(30));
            System.out.println(Thread.currentThread().getName() + " " + ar.get(ar.size() - 1));
            countDecrease();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }
    }
    public synchronized void countDecrease() {
        --counter;
    }
}

class ReadAndWrite {
    public static void main(String[] args) {
        ArrayList<Integer> ar = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        for (Integer i: ar) {
            ar.add(i + 1);
        }
        System.out.println(ar);
    }
}
class ReadAndWriteCorrect {
    public static void main(String[] args) {
        List<Integer> ar = new CopyOnWriteArrayList<>(Arrays.asList(1, 2, 3, 4));

        List<Integer> list = Collections.synchronizedList(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        for (Integer i: ar) {
            ar.add(i + 1);
        }
        System.out.println(ar);

        for (Integer i: list) {
            list.add(i + 2);
        }
        System.out.println(list);
    }
}

@Log
class Mapping {
    static ConcurrentHashMap<Integer, Integer> map = new ConcurrentHashMap<>();
    @SneakyThrows
    public static void main(String[] args) {
        map.put(1, 2);
        map.put(2, 3);
        map.put(3, 4);
        for (Map.Entry<Integer, Integer> entry: map.entrySet()) {
            map.put(entry.getKey() * 2, entry.getValue() * 2);
        }
        System.out.println(map);
    }
}
