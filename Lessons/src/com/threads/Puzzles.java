package com.threads;

import java.util.HashMap;
import java.util.Map;

public class Puzzles {
    public static void main(String[] args) {
        Map<String, String> numbersMap = new HashMap<>();
        Map<String, String> stringMap = new HashMap<>();
        Map<String, String> m1 = stringMap;
        m1.put("a", "aaa");
        m1.put("b", "bbb");
        Map<String, String> m2 = numbersMap;
        m2.put("a", "111");
        m2.put("b", "222");
        m2.replaceAll(m1::put);
        System.out.println(m1);
        System.out.println(m2);
    }
}

