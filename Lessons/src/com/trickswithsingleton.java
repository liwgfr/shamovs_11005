package com;

public class trickswithsingleton {

}

class Singleton1 { // first bad variant. Создается всегда, даже если getInstance не вызван
    private static final Singleton1 instance = new Singleton1();
    public static Singleton1 getInstance() {
        return instance;
    }
    private Singleton1() { }

}
class Singleton2 { // second bad+ variant. Что с многопоточкой?
    private static Singleton2 instance;

    public static Singleton2 getInstance() {
        if (instance == null) {
            instance = new Singleton2();
        }
        return instance;
    }
    private Singleton2() { }

}
class Singleton3 { // third bad++ variant. несколько потоков ломанутся создавать синглтоны создадутся лишние
    private static volatile Singleton3 instance;

    public static Singleton3 getInstance() {
        if (instance == null) {
            instance = new Singleton3();
        }
        return instance;
    }
    private Singleton3() { }

}
class Singleton4 { // fourth normal variant. несколько потоков ломанутся создавать синглтоны создадутся лишние
    private static Singleton4 instance; // вышли из замка и захватываем следующий замок тогда видно состояние. Волатайл не нужен
    public static synchronized Singleton4 getInstance() { // lock contention
        if (instance == null) {
            instance = new Singleton4();
        }
        return instance;
    }
    private Singleton4() { }

}

class Singleton5 { // fifth good variant. запись синхронизирована, а чтение - нет. Частично сконструированный объект может быть доступен другому треду
    private static Singleton5 instance; // вышли из замка и захватываем следующий замок тогда видно состояние. Волатайл не нужен
    private static final Object lock = new Object();
    public static Singleton5 getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) { // если два потока придут. Один создаст, другой увидит и не будет создавать
                    instance = new Singleton5();
                }
            }
        }
        return instance;
    }
    private Singleton5() { }

}
class Singleton6 { // sixth good+ variant. Фиксим предыдущую ошибку и добавляем volatile
    private static volatile Singleton6 instance;
    private static final Object lock = new Object();
    public static Singleton6 getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) { // если два потока придут. Один создаст, другой увидит и не будет создавать
                    instance = new Singleton6();
                }
            }
        }
        return instance;
    }
    private Singleton6() { }

}
class Singleton7 { // seventh good++ variant. Effective Java (x1.4). Ошибка - первый поток будет видеть null
    private static volatile Singleton7 instance;
    private static final Object lock = new Object();
    public static Singleton7 getInstance() {
        Singleton7 result = instance;
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) { // если два потока придут. Один создаст, другой увидит и не будет создавать
                    result = instance;
                    instance = new Singleton7();
                }
            }
        }
        return result;
    }
    private Singleton7() { }

}
class Singleton { // the best lazy variant.
    private static volatile Singleton instance;
    private static final Object lock = new Object();
    public static Singleton getInstance() {
        Singleton result = instance;
        if (result != null) {
            return result;
        }
            synchronized (lock) {
                if (instance == null) {
                    instance = new Singleton();
                }
                return instance;
            }
    }
    private Singleton() { }
}

class Singleton8 { // the best variant too. It's lazy

    private static class SingletonHolder {
        public static final Singleton8 instance = new Singleton8();
    }
    public static Singleton8 getInstance() {
        return SingletonHolder.instance;
    }
    private Singleton8() {}
}
