import java.util.ArrayList;
import java.util.Scanner;

public class Tasker {
    static ArrayList<Integer> b = new ArrayList<>();
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] ar = new int[n];

        for (int i = 0; i < n; i++) {
            ar[i] = in.nextInt();
            b.add(1);
        }
        for (int i = n - 1; i >= 0; i--) {
            System.out.println(ar); // {2, 0 ,0}
            // { 2 = ar[i+1] = 0
            // b = {1, 1, 1}
            ar[i] = get(ar[i] + 1, n);
        }
        for (int j : ar) {
            System.out.print(j + " ");
        }
    }

    static int get(int x, int n) {
        // 0
        for (int i = n - 1; i >= 0; i--) {
            // 2, 0, 0
            x = x - b.get(i); // 0 = 0 - 1,
            if (x <= 0) {
                b.set(i, 0); // {0, 1, 1}
                return i + 1; //
            }
        }
        return 0;
    }
}
