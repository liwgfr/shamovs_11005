import java.lang.annotation.Annotation;

public class Class implements ann {

    @Override
    public String value() {
        return "Bless";
    }

    @Override
    public java.lang.Class<? extends Annotation> annotationType() {
        return ann.class;
    }

    public void launch() {
        System.out.println(this.value());
    }
}

class Main {
    public static void main(String[] args) {
        Class cl = new Class();
        cl.launch();
    }
}
