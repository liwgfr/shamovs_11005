import java.util.Scanner;

public class ScannerDecorator extends DataSourceDecorator{

    public ScannerDecorator(Scanner source) {
        super(source);
    }

    public int writeData(Scanner data) {
        return super.writeData(data);
    }
    public String  writeStringData(Scanner data) {
        return super.writeStringData(data);
    }

    public Scanner readData() {
        return super.readData();
    }


}
