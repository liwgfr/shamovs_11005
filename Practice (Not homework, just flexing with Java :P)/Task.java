import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task {
    public static void main(String[] args) {
        Shape rectangle = new Rectangle(1, 5);
        Shape circle = new Circle(5);
        System.out.println(rectangle);
        System.out.println(circle);
        int []ar = new int[] {5, 10, 2, 0, 13, -2, 51};
        System.out.println(MinMax.minmax(ar));
    }
}

sealed interface Shape permits Circle, Rectangle, Square {}
non-sealed class Square implements Shape { // non-sealed для всех! sealed - только для permits, final - никто

}
record MinMax() {
    record Limits(int min, int max) {}
    public static Limits minmax(int[] elements) {

        List<Integer> list = Arrays.stream(elements).boxed().collect(Collectors.toList());
        return new Limits(list.stream().min(Integer::compareTo).orElse(0), list.stream().max(Integer::compareTo).orElse(0));
    }
}

final record Rectangle(int width, int heigth) implements Shape {}
