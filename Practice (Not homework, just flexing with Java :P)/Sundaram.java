import java.util.Arrays;
import java.util.Scanner;

public class Sundaram {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int nNew = (n - 1) / 2;
        boolean[] mark = new boolean[nNew + 1];

        Arrays.fill(mark, false);

        for (int i = 1; i <= nNew; i++) {
            for (int j = 1; (i + j + 2 * i * j) <= nNew ; j++) {
                    mark[i + j + 2 * i * j] = true;
            }
        }
        if (n > 2) {
            System.out.print(2 + " ");
            for (int i = 1; i <= nNew; i++) {
                if (!mark[i]) {
                    System.out.print(2 * i + 1 + " ");
                }
            }
        }

    }
}
