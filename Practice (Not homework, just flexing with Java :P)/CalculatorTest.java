import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    public void setUp() {
        calculator = new Calculator();
    }


    @Test
    @DisplayName("Should work")
    public void testMultiply() {
        Assertions.assertEquals(20, calculator.multiply(4, 5), "Regular multiply should work");
    }
    @RepeatedTest(5)
    @DisplayName("Ensure correct handling zero")
    public void testMultiplyZero() {
        Assertions.assertEquals(0, calculator.multiply(0, 5), "Multiply with zero should be zero");
        Assertions.assertEquals(0, calculator.multiply(5, 0), "Multiply with zero should be zero");
    }
}
