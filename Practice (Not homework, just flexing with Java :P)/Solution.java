import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] a = {1, 2, 3, 10, 100};
        int[] b = {2, 5, 7, 13, 101};
        ArrayList<Integer> ar = new ArrayList<>();
        for (int j : a) {
            ar.add(j);
        }
        for (int j : b) {
            ar.add(j);
        }
        LinkedHashSet<Integer> set = new LinkedHashSet<>(ar);
        System.out.println(set);
    }
}