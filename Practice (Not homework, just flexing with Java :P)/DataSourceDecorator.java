import java.util.Scanner;

public class DataSourceDecorator {
    private final Scanner wrappee;

    public DataSourceDecorator(Scanner source) {
        this.wrappee = source;
    }

    public int writeData(Scanner data) {
            int v = wrappee.nextInt();
            wrappee.nextLine();
            return v;
    }

    public String writeStringData(Scanner data) {
        return wrappee.nextLine();
    }

    public Scanner readData() {
        return wrappee;
    }
}
