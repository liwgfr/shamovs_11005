import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class Multithreading  {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        List<Future<String>> futures = new ArrayList<>();
        Callable<String> callable = () -> Thread.currentThread().getName();

        for (int i = 0; i < 3; i++) {
            Future<String> future = executorService.submit(callable);
            futures.add(future);
        }
        System.out.println(futures);
        for (Future<String> f:
             futures) {
            System.out.println(f.get());
        }
        executorService.shutdown();
    }
}


class Exec {
    AtomicInteger sum = new AtomicInteger(0);
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 30; i++) {
            Exec exec = new Exec();
            exec.run(exec);
        }
    }

    public void run(Exec exec) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        IntStream.range(0, 1000000).parallel().forEach(x -> executorService.submit(exec::synchCalc));

        executorService.shutdown();

        if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
            executorService.shutdownNow();
        }

        System.out.println(sum.get());
    }
    public synchronized void synchCalc() {
        sum.incrementAndGet();
    }
}

class Lesson extends Thread {
    Semaphore semaphore = new Semaphore(1);
    boolean flag = false;
    AtomicInteger num = new AtomicInteger();

    public static void main(String[] args) {
        new Lesson().start();
    }
    @SneakyThrows
    public void run() {
        while (true) {
            if (!flag) {
                semaphore.acquire();
                int out = new Random().nextInt();
                num.set(out);
                System.out.println("Отправил: " + out);
                sleep(500);
                semaphore.release();
                flag = true;
            } else {
                semaphore.acquire();
                System.out.println("Получил: " + num.get());
                sleep(500);
                semaphore.release();
                flag = false;
            }
        }
    }
}
