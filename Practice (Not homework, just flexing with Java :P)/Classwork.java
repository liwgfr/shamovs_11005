import java.util.Scanner;

public class Classwork {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Без декоратора");
        int x = in.nextInt();
        System.out.println(x);
        String m = in.nextLine();
        System.out.println(m);
        System.out.println("Конец");
        System.out.println("С декоратором: ");

        DataSourceDecorator dataSourceDecorator = new ScannerDecorator(in);
        int f = dataSourceDecorator.writeData(in);
        System.out.println(f);
        String c = dataSourceDecorator.writeStringData(in);
        System.out.println(c);
        System.out.println("Конец");


    }
}
