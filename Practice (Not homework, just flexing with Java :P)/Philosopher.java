import lombok.SneakyThrows;

import java.util.concurrent.Semaphore;



class Starter {
    public static void main(String[] args) {

        Semaphore semaphore = new Semaphore(2);
        new Thread(new Philosopher(semaphore, 1)).start();
        new Thread(new Philosopher(semaphore, 2)).start();
        new Thread(new Philosopher(semaphore, 3)).start();
        new Thread(new Philosopher(semaphore, 4)).start();
        new Thread(new Philosopher(semaphore, 5)).start();
    }
}
public class Philosopher extends Thread {
    Semaphore sem;
    int num = 0;
    int id;
    Philosopher(Semaphore sem, int id) {
        this.sem = sem;
        this.id = id;
    }

    @SneakyThrows
    public void run() {
        while (num < 3) {
            sem.acquire();
            System.out.println("Philosopher " + id + " is eating");
            sleep(500);
            num++;
            System.out.println("Philosopher " + id + " ended up");
            sem.release();
            sleep(500);
        }
    }
}

