package com.to220221;

import java.util.Arrays;
import java.util.List;

public abstract class Tuple {
    private final Object[] valueArray;
    private final List<Object> valueList;

    protected Tuple(Object... values) {
        this.valueArray = values;
        this.valueList = Arrays.asList(values);
    }

    public abstract int getSize();

    public final Object getValue(int position) {
        if (position >= this.getSize()) {
            throw new IllegalArgumentException();
        } else {
            return this.valueArray[position];
        }
    }


    public final String toString() {
        return this.valueList.toString();
    }
}
