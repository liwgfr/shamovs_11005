package com.to220221;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

public class Range implements Iterable<Integer> {
    private final int length;
    protected int counter;

    public Range(int length) {
        this.length = length;
        counter = 0;
    }

    public @NotNull Iterator<Integer> iterator() {
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return counter < length;
            }

            @Override
            public Integer next() {
                return counter++;
            }
        };
    }
}
