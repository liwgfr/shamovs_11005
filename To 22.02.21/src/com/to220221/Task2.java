package com.to220221;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Range range = new Range(5);
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String v = in.nextLine();
        System.out.println(n);
        System.out.println(v);
        while (range.iterator().hasNext()) {
            System.out.print(range.iterator().next() + " ");
        }
        RangeInterval rangeInterval = new RangeInterval(5, 7);
        while (rangeInterval.iterator().hasNext()) {
            System.out.print(rangeInterval.iterator().next() + " ");
        }
        System.out.println();
        FibonacciRange fibonacciRange = new FibonacciRange(11);
        while (fibonacciRange.iterator().hasNext()) {
            System.out.print(fibonacciRange.iterator().next() + " ");
        }

    }
}
