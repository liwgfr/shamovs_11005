package com.to220221;

public class Quatro<T, Z, Q, W> extends Tuple {
    private static final int SIZE = 4;
    private final T val1;
    private final Z val2;
    private final Q val3;
    private final W val4;

    public Quatro(T val1, Z val2, Q val3, W val4) {
        super(val1, val2, val3, val4);
        this.val1 = val1;
        this.val2 = val2;
        this.val3 = val3;
        this.val4 = val4;
    }
    public T getValue1() {
        return this.val1;
    }

    public Z getValue2() {
        return this.val2;
    }

    public Q getValue3() {
        return this.val3;
    }

    public W getValue4() {
        return this.val4;
    }

    public int getSize() {
        return 4;
    }

}
