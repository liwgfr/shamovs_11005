package com.to220221;

public class RangeInterval extends Range{
    public RangeInterval(int start, int length) {
        super(Math.max(length, start));
        if (start == length) {
            throw new IllegalArgumentException("You have inserted same values. It will print nothing");
        }
        counter = Math.min(length, start);
    }
}
