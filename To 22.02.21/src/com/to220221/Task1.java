package com.to220221;

import org.javatuples.Quartet;

import java.util.ArrayList;
import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        Integer[] a = {1, 512, 23, 100, 22, 5};
        Quartet<String, ArrayList<Integer>, Boolean, Integer> quartet = new Quartet<>("Hello", new ArrayList<>(Arrays.asList(a)), true, 100);
        System.out.println("Quartet works!\n" + quartet); // Quartet юзая фрейм из Maven

        // Свой:

        Quatro<String, ArrayList<Integer>, Boolean, Integer> quatro = new Quatro<>("MyOwnClass", new ArrayList<>(Arrays.asList(a)), false, 228);
        System.out.println(quatro);
    }
}
