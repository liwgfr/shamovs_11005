package com.to220221;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FibonacciRange{
    private final int length;
    protected int counter;
    private final ArrayList<Integer> ar;

    public FibonacciRange(int length) {
        this.length = length;
        counter = 1;
        ar = Stream.iterate(new int[]{0, 1}, arr -> new int[]{arr[1], arr[0] + arr[1]})
                .limit(length)
                .map(y -> y[0]).collect(Collectors.toCollection(ArrayList::new));
    }
    public @NotNull Iterator<Integer> iterator() {
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return counter < length;
            }

            @Override
            public Integer next() {
                return ar.get(counter++);
            }
        };
    }
}
