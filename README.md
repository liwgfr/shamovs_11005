# Домашние задания по программированию (группа 11-005)

-----
## Каждая папка **вида "To DD_MM_YY"** соответствует числу, к которому данная домашнаяя работа выполнена.
_____
# Контакты
______
*Мой телеграм: [@binocla](https://t.me/binocla)* 
----
*Мой ВК: [@binocla](https://vk.com/id453785318)*
-------
## Ссылка на AISD репозиторий: https://bitbucket.org/liwgfr/aisd_shamov11005/src/master/
## Ссылка на домашку с ботом (Maven + Dockerfile + Heroku): https://github.com/liwgfr/homework
-------
## Java Version: 16.0.2 (openjdk) ##
Additional info: openjdk 16-ea 2021-03-16
OpenJDK Runtime Environment (build 16-ea+35-Debian-1)
OpenJDK 64-Bit Server VM (build 16-ea+35-Debian-1, mixed mode, sharing)
## Project Language Level: 16 (Preview) ##