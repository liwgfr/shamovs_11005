package com.to180321;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Task 1");
        String[] k = in.nextLine().toLowerCase(Locale.ROOT).split(" ");
        Map<String, Integer> map = new HashMap<>();
        for (String w : k) {
            Integer n = map.get(w);
            n = (n == null) ? 1 : ++n;
            map.put(w, n);
        }
        System.out.println(map);
        System.out.println("Task 1 ended");
        System.out.println("Task 2");
        Map<String, Map<String, Integer>> l = new HashMap<>();
        for (int i = 0; i < 5; i++) {
            String[] s = in.nextLine().split(" ");
            Map<String, Integer> f = new HashMap<>();
            if (l.containsKey(s[0])) {
                f = l.get(s[0]);
            }
            f.put(s[1], Integer.parseInt(s[2]));
            l.put(s[0], f);
        }
        for (Map.Entry<String, Map<String, Integer>> m : l.entrySet()) {
            System.out.println(m.getKey() + ":");
            for (Map.Entry<String, Integer> x : m.getValue().entrySet()) {
                System.out.println(x.getKey() + " " + x.getValue());
            }
        }
    }
}
