package com.to130521;

import lombok.SneakyThrows;
import lombok.extern.java.Log;

import java.io.File;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

@Log
public class Main {
    @SneakyThrows

    public static void main(String[] args) {
        log.info("First task started");
        File file = new File("/home/binocla/IdeaProjects/shamovs_11005/To 13.05.21/src/com/to130521/input.txt");
        FileReader fileReader = new FileReader(file);
        Scanner in = new Scanner(fileReader);
        Map<String, Integer> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        while (in.hasNextLine()) {
            String[] l = in.nextLine().split("\\|");
            if (map.containsKey(l[0])) {
                map.put(l[0], map.get(l[0]) + Integer.parseInt(l[1]));
            } else {
                map.put(l[0], 0);
            }

        }
        list.stream().filter(x -> x.charAt(0) == 'a')
                .sorted(Comparator.comparing(String::length))
                .forEach(System.out::println);
        log.info("First task ended");
        log.info("Second task started");
        int[] a = new int[] {123, 231, 312, 592, 155, 251, 199, 912, 902, 130};
        List<String> ar = new ArrayList<>();
        for (int j : a) {
            ar.add(String.valueOf(j));
        }
       Comparator<String> comparator = Comparator.comparingInt(o -> o.charAt(o.length() - 1) - '0');
        HashSet<String> set = ar.stream().sorted(comparator).map(x -> x.substring(0, x.length() - 1)).collect(Collectors.toCollection(HashSet::new));
        System.out.println(set);
        log.info("Second task ended");
        log.info("Third task started");
        System.out.println(map);
        log.info("Third task ended");
    }
}
