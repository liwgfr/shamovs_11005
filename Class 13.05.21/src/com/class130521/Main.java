package com.class130521;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Purchase purchase1 = new Purchase("Alex", "Cola", "Didi", 3);
        Purchase purchase5 = new Purchase("Alex", "Cola", "Magnit", 3);
        Purchase purchase2 = new Purchase("Jack", "Pepsi", "5", 13);
        Purchase purchase3 = new Purchase("Max", "Sprite", "Magnit", 2);
        Purchase purchase4 = new Purchase("Vlad", "Mirinda", "Popi", 100);

        Product product1 = new Product("Cola", "Didi", 133);
        Product product3 = new Product("Cola", "Magnit", 500);
        Product product2 = new Product("Sprite", "Magnit", 200);

        Purchase[] purchases = new Purchase[] {purchase1, purchase2, purchase3, purchase4, purchase5};
        Product[] products = new Product[] {product1, product2, product3};
        System.out.println(task1(purchases));
        System.out.println(task2(purchases, products));
        System.out.println(task3(purchases, products));
        System.out.println(task4(purchases, products));
    }

    public static String task1(Purchase[] purchases) { // groupBy
        return Stream.of(purchases).sorted(Comparator.comparing(Purchase::count).reversed())
                .collect(Collectors.toCollection(ArrayList::new)).get(0).buyer();
    }

    public static String task2(Purchase[] purchases, Product[] products) {
        HashMap<String, Integer> map = new HashMap<>();
        Stream.of(purchases).forEach(x -> {
           for (Product product:
                products) {
               if (x.productName().equals(product.name())) {
                   if (map.containsKey(x.buyer())) {
                       map.put(x.buyer(), map.get(x.buyer()) + product.price());
                   } else {
                       map.put(x.buyer(), product.price());
                   }
               }
           }
       });
       return map.toString();
    }

    public static String task3(Purchase[] purchases, Product[] products) {
        HashMap<String, Integer> map = new HashMap<>();
        Map<String, Integer> mapOfInside = new HashMap<>();
        Stream.of(purchases).forEach(x -> {
            for (Product product:
                    products) {
                if (x.productName().equals(product.name())) {
                    if (mapOfInside.containsKey(product.name())) {
                        int curr = mapOfInside.get(product.name());
                        if (curr <= product.price()) {
                            mapOfInside.put(product.name(), product.price());
                        }
                    } else {
                        mapOfInside.put(product.name(), product.price());
                    }
                    if (map.containsKey(x.productName())) {
                        map.put(x.productName(), map.get(x.productName()) + 1);
                    } else {
                        map.put(x.productName(), 1);
                    }
                }
            }
        });
        ArrayList<String> ar = new ArrayList<>();
        for (int i = 0; i < mapOfInside.size(); i++) {
            ar.add(mapOfInside.keySet().toArray()[i].toString() + "-" + map.values().toArray()[i].toString()
                    + "-" + mapOfInside.values().toArray()[i].toString());
        }
        return ar.toString();
    }

    public static String task4(Purchase[] purchases, Product[] products) {
        HashMap<String, Integer> map = new HashMap<>();
        Map<String, Integer> mapOfInside = new HashMap<>();
        Stream.of(purchases).forEach(x -> {
            for (Product product:
                    products) {
                if (x.shop().equals(product.shop())) {
                    if (mapOfInside.containsKey(x.buyer())) {
                            mapOfInside.put(x.buyer(), mapOfInside.get(x.buyer()) + product.price());
                    } else {
                        mapOfInside.put(x.buyer(), product.price());
                    }
                    if (map.containsKey(x.shop())) {
                        map.put(x.shop(), map.get(x.shop()) + product.price());
                    } else {
                        map.put(x.shop(), product.price());
                    }
                }
            }
        });
        ArrayList<String> ar = new ArrayList<>();
        for (int i = 0; i < mapOfInside.size(); i++) {
            ar.add(map.keySet().toArray()[i].toString() + "-" + mapOfInside.keySet().toArray()[i].toString()
                    + "-" + mapOfInside.values().toArray()[i].toString());
        }
        return ar.toString();
    }
}

record Product(String name, String shop, int price) {
}
record Purchase(String buyer, String productName, String shop, int count) {
}
