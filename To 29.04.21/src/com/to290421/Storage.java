package com.to290421;

import lombok.extern.java.Log;

@Log
public record Storage(String name, int amount) { }
