package com.to290421;

import lombok.extern.java.Log;

@Log
public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage("Coca-Cola", 12);
        Storage storage1 = new Storage("Pepsi-Cola", 5);
        StorageController storageController = new StorageController(storage, storage1);
        storageController.addToStorage(new Storage("Cheese", 3));
        storageController.output();
        storageController.deleteFromStorage(storage);
        storageController.output();
        storageController.changeStorage(storage1, 50);
        storageController.output();
    }
}
