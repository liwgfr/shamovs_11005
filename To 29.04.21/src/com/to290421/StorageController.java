package com.to290421;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import lombok.extern.java.Log;
import org.bson.Document;

@Log
public class StorageController {
    // @Config for Database connectivity
    private final MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017/");
    private final MongoDatabase database = mongoClient.getDatabase("storage");
    private final MongoCollection<Document> collection = database.getCollection("list");

    // @Constructor for Database
    public StorageController(Storage... args) {
        for (Storage s: args) {
            Document storage = new Document(s.name(), s.amount());
            collection.insertOne(storage);
        }
        log.severe("Database successfully fulfilled");
    }

    // @addToStorage to add new item
    public void addToStorage(Storage storage) {
        collection.insertOne(new Document(storage.name(), storage.amount()));
        log.info("Successfully inserted!");
    }


    // @deleteFromStorage to delete an item
    public void deleteFromStorage(Storage storage) {
        collection.deleteOne(Filters.eq(storage.name(), storage.amount()));
        log.info("Successfully deleted!");
    }

    // @changeStorage to change the amount
    public void changeStorage(Storage storage, int amount) {
        Document setData = new Document();
        setData.append(storage.name(), amount);
        collection.findOneAndReplace(Filters.eq(storage.name(), storage.amount()), setData);
        log.info("Successfully updated!");
    }
    // @output to output all items in the database
    public void output() {
        FindIterable<Document> iter = collection.find();
        for (Document document : iter) {
            log.info(document.toJson());
        }
    }
}
