package com.threads;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class ThreadStatusTest {
    @Test
    void runThreads() {
        ArrayList<Integer> ar = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            new Thread(() -> ar.add((int) Thread.currentThread().getId())).start();
        }
        int start = 16;
        for (int i = 0; i < ar.size(); i++) {
            Assertions.assertEquals(start, ar.get(i).intValue());
            start++;
        }
    }
}