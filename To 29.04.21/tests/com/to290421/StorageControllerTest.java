package com.to290421;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StorageControllerTest {
    @Mock
    private  MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017/");
    @Mock
    private  MongoDatabase database = mongoClient.getDatabase("storage");
    @Mock
    private  MongoCollection<Document> collection = database.getCollection("list");

    private static Storage storage;
    @BeforeAll
    public void init() {
        storage = new Storage("Lay's: Sour Cream", 2);
    }


    @Test
    void addToStorage() {
        Document document = new Document(storage.name(), storage.amount());
        collection.insertOne(document);
    }

    @AfterAll
    void deleteFromStorage() {
        collection.deleteOne(Filters.eq(storage.name(), storage.amount()));
    }

    @Test
    void changeStorage() {
        int amount = 5;
        Document setData = new Document(storage.name(), amount);
        setData.append(storage.name(), amount);
        collection.findOneAndReplace(Filters.eq(storage.name(), storage.amount()), setData);
    }

    @Test
    void output() {
        FindIterable<Document> iter = collection.find();
        for (Document document : iter) {
            document.toJson();
        }
    }
}