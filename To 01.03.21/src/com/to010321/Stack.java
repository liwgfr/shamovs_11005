package com.to010321;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;

public class Stack<T> {
    protected final ArrayList<T> stackArr;
    private int head;

    public Stack() {
        stackArr = new ArrayList<>();
        head = -1;
    }

    public void push (T thing) {
        stackArr.add(++head, thing);
    }

    public T pop() {
        return stackArr.remove(head);
    }

    public T peek() { return stackArr.get(head); }

    public boolean isEmpty() {
        return (head == -1);
    }

    public Object[] toArray() {
        return stackArr.toArray();
    }

    public void forEach() {
        StackIterator<T> stackIterator = new StackIterator<>(this);
        while (stackIterator.iterator().hasNext()) {
            System.out.println(stackIterator.iterator().next() + " ");
        }
    }

}

class StackIterator<T> extends Stack<T> implements Iterable<T>  {
    protected final Stack<T> stack;
    private final int length;
    protected int counter;

    public StackIterator(Stack<T> stack) {
        this.stack = stack;
        this.length = stack.stackArr.size();
        counter = 0;
    }



    public @NotNull Iterator<T> iterator() {

        return new Iterator<>() {
            @Override
            public boolean hasNext() {

                return counter < length;
            }

            @Override
            public T next() {
                return stack.stackArr.get(counter++);
            }
        };
    }
}
