package com.to010321;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

public class LinkedList<T> implements Iterable<T>  {
    protected int counter;
    protected int length;
    public LinkedList() {
        length = 0;
        counter = 0;
        this.length = getCount();
    }


    @NotNull
    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return counter < length;
            }

            @Override
            public T next() {
                counter++;
                T result = head.item;

                if (head.end())
                    head = head.next;
                return result;
            }
        };
    }
    public void forEach() {
        while (this.iterator().hasNext()) {
            System.out.println(iterator().next());
        }
    }
    public int getCount() {
        return length;
    }
    private static class Node<T> {
             final T item;
            final Node<T> next;

            Node() {
                item = null;
                next = null;
            }

            Node(T item, Node<T> next) {
                this.item = item;
                this.next = next;
            }

            boolean end() { return item != null || next != null; }
    }

    private Node<T> head = new Node<>();

    public void push(T item) {
        length++;
        head = new Node<>(item, head);
    }

    public T pop() {
        T result = head.item;

        if (head.end())
            head = head.next;

        return result;
    }

    public Object peek() {
        return head.item;
    }

}

