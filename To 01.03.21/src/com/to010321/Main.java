package com.to010321;

public class Main {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        System.out.println("Stack");
        stack.push(5);
        stack.push(3);
        stack.push(1);
        stack.push(2);
        stack.forEach();
        System.out.println("Linked list: ");
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.push(3);
        linkedList.push(123);
        linkedList.push(5);
        linkedList.forEach();

    }
}
