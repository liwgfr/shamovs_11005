package com.to080321;

import java.util.Scanner;
import java.util.Stack;

public class Calculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        if (s.charAt(s.length() - 1) != ' ') {
            s += ' ';
        }
        Stack<Integer> values = new Stack<>();
        int last = 0;
        int second;
        String item;
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) == ' ') {
                item = s.substring(last, i);
                item = item.replaceAll(" ", "");

                switch (item) {
                    case "+" -> values.push(values.pop() + values.pop());
                    case "-" -> {
                        second = values.pop();
                        values.push(values.pop() - second);
                    }
                    case "*" -> values.push(values.pop() * values.pop());
                    case "/" -> {
                        second = values.pop();
                        values.push(values.pop() / second);
                    }
                    default -> values.push(Integer.parseInt(item));
                }
                last = i;
            }
        }
        System.out.println(values.peek());
    }
}
