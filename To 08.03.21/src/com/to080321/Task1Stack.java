package com.to080321;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;

public class Task1Stack {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Deque<Character> deque = new LinkedList<>();
        String s = in.next();
        boolean flag = true;
        for (char c: s.toCharArray()) {
            if (c == '{' || c == '(' || c == '[') {
                deque.addFirst(c);
            } else {
                if (!deque.isEmpty() && ((deque.peekFirst() == '{' && c == '}')
                        || (deque.peekFirst() == '[' && c == ']')
                        || (deque.peekFirst() == '(' && c == ')'))) {
                    deque.removeFirst();
                } else {
                    flag = false;
                    break;
                }
            }
        }
        System.out.println(flag);
    }
}
