package com.to250321;

import lombok.SneakyThrows;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<>();
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Enter 1 to Exit\nEnter 2 to Enter New Product and its Amount\n" +
                    "Enter 3 to Show All Products\n" +
                    "Enter 4 to Delete Product\n" +
                    "Enter 5 to Change amount of Product");
            String s = in.next();
            if (s.equals("1")) {
                break;
            }
            if (s.equals("2")) {
                FileWriter fileWriter = new FileWriter("output.txt", false);
                in.nextLine();
                String[] ar = in.nextLine().split(" ");
                map.put(ar[0], Integer.parseInt(ar[1]));
                fileWriter.write(map.toString());
                fileWriter.flush();
                fileWriter.close();

                System.out.println("Successfully Added!");
            }
            if (s.equals("3")) {
                FileReader file = new FileReader("output.txt");
                Scanner reader = new Scanner(file);
                System.out.println("-------------------");
                while (reader.hasNextLine()) {
                    System.out.println(reader.nextLine());
                }
                System.out.println("-------------------");
            }
            if (s.equals("4")) {
                String x = in.next();
                map.remove(x);
                FileWriter fileWriter = new FileWriter("output.txt", false);
                fileWriter.write(map.toString());
                fileWriter.flush();
                fileWriter.close();
                System.out.println("Successfully deleted!");
            }
            if (s.equals("5")) {
                String x = in.next();
                int v = in.nextInt();
                map.put(x, v);
                FileWriter fileWriter = new FileWriter("output.txt", false);
                fileWriter.write(map.toString());
                fileWriter.flush();
                fileWriter.close();
                System.out.println("Successfully changed!");
            }
        }

    }
}
