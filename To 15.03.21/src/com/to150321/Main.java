package com.to150321;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Stream;

class Person {
    public String name;
    public int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
    public int compareTo (Person person) {
        return compare(this, person);
    }
    public static int compare(Person x, Person y) {
        return (x.age < y.age) ? -1 : ((x == y) ? 0 : 1);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

public class Main {
    public static void main(String[] args) {
        ArrayList<Person> arrayList = new ArrayList<>();
        arrayList.add(new Person("Mike", 12));
        arrayList.add(new Person("John", 3));
        arrayList.add(new Person("Steven", 2 ));
        arrayList.sort(Person::compareTo);
        System.out.println(arrayList.toString());
        Stream<Integer> stream = Stream.<Integer>builder().build();
        Stream<Integer> stream2 = Stream.<Integer>builder().build();
        stream = Stream.of(3, 1, 5);
        stream2 = Stream.of(1, 3);
        System.out.println(stream);
        System.out.println(stream2);
        ArrayList<Stream<Integer>> arrayList1 = new ArrayList<>();
        arrayList1.add(stream);
        arrayList1.add(stream2);
    }
}
